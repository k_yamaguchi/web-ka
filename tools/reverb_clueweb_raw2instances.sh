#!/bin/bash


function fmt_reverb_raw {
    file=$1
    zcat $file |
    awk '
    BEGIN {
        FS = OFS = "\t"
    }
    {
        score = $7
        argc = 2
        arg1 = $4
        rel = $5
        arg2 = $6

        print score, source, rel, argc, arg1, arg2
    }' source=$file
}

for f in $*; do
    fmt_reverb_raw $f
done
